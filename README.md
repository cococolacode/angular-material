## Getting Started

### Installation 
> using npm
``` terminal
npm install --save @angular/material
```
## Configure
Add the following line in your angular.json file as value with a style key,
``` javascript
"styles":{
    "./node_modules/@angular/material/prebuilt-themes/indigo-pink.css",
   }
  ```
  >By default the theme is indio-pick, you can also change it manually.
  
 Import BrowserAnimationModule at app.module.ts, _because some of the components provided by angular material uses animations._
 ``` javascrip
 import {BrowserAnimationModule} from '@angular/platform-browser/animations'
 ```
 

If you are using angular 6+, there is another way of installing it,
``` terminal
ng add @angular/material
```
_It will not only install the angular material, but also already configure the project to included._


## Uses
By default non of the components provided by the angular material were available, this is done to save spaces.
we have to unlock those component to use it in our project. 
Let's unlock @angular/material input field.
At app.module.ts,
``` javascript
import {MatInputModule} from '@angular/material'
```
_To unlock it,_ 
``` 
imports[
 .....,
MatInputModule
]
```
Now , we can use the material input field inside any components.
**how to use it?** https://material.angular.io/components/input/overview
